## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).


## Setup instructions

Git instructions: 

1. Make sure you have git installed in the pc 
2. from the command line clone the project using " git clone -b test-mohamedAnsari git@gitlab.com:mohamedansari/tryouts.git" 
3. Run the composer inside the tryouts file using the command " composer install " 
4. Copy .env.example file to .env on root folder. You can type copy .env.example .env if using command prompt Windows or cp .env.example .env if using terminal Ubuntu
4. generate new application key from the command line using "  php artisan key:generate  " 

Database: 
1.create new database with the name tryouts 

laravel instructions: 
1. change the variables in the " .env " file
		- DB_DATABASE=
		- DB_USERNAME=
		- DB_PASSWORD=
		
2. migrate and seed the database using " php artisan migrate --seed " 
3. run laravel from the command line using " php artisan serve " 

Scheduling instructions: 
using linex: 
1. follow the instructions on the  laravel page: https://laravel.com/docs/5.1/scheduling
on windows 
2. follow the instructions on the page: http://s4.jeffsbio.co.uk/laravel-5-scheduler-set-up-cron-on-windows


##note
make sure all the folder have the right permissions 


## Tasks Times

1. Booking parser
	- understaing the task: 20 hours
	- completing the task: 4 hours 
2. Users list
	- approximately: 1 hours  
3. Booking list
	- approximately: 1 hours  
4. design
	- 30 minutes 
5. laravel setup 
	- approximately: 1 hours