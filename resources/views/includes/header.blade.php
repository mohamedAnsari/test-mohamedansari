<!-- Static navbar -->
 <nav class="navbar navbar-default navbar-static-top">
   <div class="container">
     <div class="navbar-header">
       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
       </button>
       <a class="navbar-brand" href="/">Tryouts</a>
     </div>
     <div id="navbar" class="navbar-collapse collapse">
       <ul class="nav navbar-nav">
         <li ><a href="/">Home</a></li>
          @if(Auth::user())
         <li><a href="/bookings">View Bookings</a></li>
         <li><a href="/users">View Users</a></li>
         @endif
       </ul>
       <ul class="nav navbar-nav navbar-right">
         @if(!Auth::user())
         <li class="dropdown">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Login <span class="caret"></span></a>
           <ul class="dropdown-menu">

             <form method="POST" action="auth/login">
               {{ csrf_field() }}
               <div class="col-md-12">
             <li>
               <div class="form-group">
                 <label for="email">Email address</label>
                 <input type="text" placeholder="Email" class="form-control" name="email">
               </div>
             </li>
             <li>
               <div class="form-group">
                 <label for="password">Password</label>
                 <input type="password" placeholder="Password" class="form-control" name="password">
               </div>
             </li>
             <li>
               <button type="submit" class="btn btn-success btn-block">Sign in</button>
             </li>
             </div>
           </form>

           </ul>
         </li>
         @else
         <li ><a>welcome admin</a></li>
         <li ><a href="/auth/logout">logout</a></li>
         @endif


       </ul>
     </div><!--/.nav-collapse -->
   </div>
 </nav>
