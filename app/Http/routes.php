<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/booking.json', 'Build\BuildController@index');
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');




//Booking routes...
Route::get('/bookings', 'booking\bookingController@index');
Route::get('/bookings/raw', 'booking\bookingController@raw');

//user routes...
Route::get('/users', 'user\userController@index');
Route::get('/users/raw', 'user\userController@raw');
