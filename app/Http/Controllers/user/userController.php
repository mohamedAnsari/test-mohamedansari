<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use View;
class userController extends Controller
{
    public function index()
    {
      //retrieve all bookings
      $users = User::get()->all();
      return View::make('user.index', array(
          'users' => $users,
      ));
    }
    public function raw()
    {
      //get and json encode the database records (first_name & last name as name from user tables, phone from user tables, country from countries tables)
      $users = json_encode(\DB::select('select CONCAT(users.first_name , " ", users.last_name) as name, phone, countries.name as country  from users, countries where users.country_id = countries.id'));
      return View::make('user.raw', array(
          'users' => $users,
      ));
    }
}
