<?php

namespace App\Http\Controllers\build;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class buildController extends Controller
{
    /**
     * show the booking.json.
     */
    public function index()
    {
      print($this->getFile('booking.json'));
    }
    /**
     * check for new changes in the remote json
     */
    public function checkUpdates(){
      //get remote booking.json content
      //open curl
      $curl = curl_init();
      $result =  file_get_contents('http://beta-tryout.malaebapp.com/bookings.json');
      $oroginalContent = $result;
      //save the file if it is new
     if($this->storeBuild($result, 'booking.json')){
       //get the file from the storage
       $bookings = $this->getFile('booking.json');
       //chech if the build hash changed
       if($this->checkBuildHash($bookings,$result)){
         //decode the json and convert it to collection
         $bookings = json_decode($bookings);
         $result = json_decode($result);
         $difference = collect();
         $resultCollect = collect($result->data);
         $bookingsCollect = collect($bookings->data);

         //spot the deleted items
         $diff = $bookingsCollect->pluck('id')->diff($resultCollect->pluck('id'))->each(function($item){

           $this->appenedToFile('bookingchanges.json',$item,'Deleted');
         });


         //spot the added items
         $diff = $resultCollect->pluck('id')->diff($bookingsCollect->pluck('id'))->each(function($item){
           $this->appenedToFile('bookingchanges.json',$item,'Added');
         });

         //spot changes in items
         $bookingsCollect->each(function($item, $key) use ($resultCollect) {

           $resultCollect->each(function($item1, $key1) use ($item)  {
            if($item->id == $item1->id) {
              if($item != $item1){
                $this->appenedToFile('bookingchanges.json',$item->id,'Update');
              }
            }
           });
         });
         \Storage::disk('local')->put('booking.json', $oroginalContent);
       }
     }
     //close curl
      curl_close($curl);

    }
    /**
    *compair the old hash build with the new hash build
    *old is the local hash build
    *new is the remote hash build
    */
    public function checkBuildHash($old, $new){
      $old = json_decode($old);
      $new = json_decode($new);
      if($old->build_hash !== $new->build_hash){
        return true;
      }
      return false;
    }

    /**
    *save the file
    */
    public function storeBuild($build,$fileName){
      //check if the file already exists
      //if it is new file it will copy the remote file to local storage
      if(!\Storage::disk('local')->has($fileName)){
        \Storage::disk('local')->put($fileName, $build);
        return false;
      }
      return true;
    }

    /**
    *get the local file
    *
    */
    public function getFile($fileName){
      return \Storage::disk('local')->get($fileName);
    }

    /**
    *append the changes to the log file
    *
    */
    public function appenedToFile($fileName,$content,$event){
      $mytime = \Carbon\Carbon::now();
      $time = $mytime->toDateTimeString();
      $data = [];
      $content = array('ID' => $content, 'Time' => $time, 'Event' => $event);
      //if the file dose not exsits create one and print the chances, else append the new changes to the file
      if(!\Storage::disk('local')->has($fileName)){
        array_push($data, $content);
        $content = json_encode($data);
        \Storage::put($fileName, $content);
      }
      else {
        $file = $this->getFile($fileName);
        $file = json_decode($file, true);
        array_push($file, $content );
        $file = json_encode($file);
        \Storage::put($fileName, $file);
      }
    }
}
